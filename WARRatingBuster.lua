--[[
	WAR Ratings Buster v0.2.5

	Ratings Buster is an addon for Warhammer: Age of Reckonging which provides extra tooltip
	information for items which modify your characters statistics.
	Also, from v 0.2 onwards allows user to enter and save/modify algorithms that
	will calculate an items usefulness/rating accordingly. Like the WoW addon Pawn

v0.2.5	11/03/10	JXW	Added in a base comparison (without talis slotted)
v0.2.3	22/09/09	JXW	Fixes to visibility defaults
v0.2.2	23/08/09	JXW	More nil checking errors and such
v0.2.1	22/08/09	JXW	Made stupid errors in v0.2 way it checks commandline and nil checking
v0.2	17/08/09	JXW	1.3.0b code. PAWN-esque attempt 1
v0.1.3	25/04/09	JXW	Removed name & item from window, couple of startup errors fixed.
v0.1.2	20/04/09	JXW	AuctionStats window aware back in
v0.1.1	18/04/09	JXW	Comparison tooltips working
v0.1.0	17/04/09	JXW	Changed mod to be easier 1.2.1 compatible.
				Reduced some output text length
v0.0.3	14/04/09	JXW	Sanitised code: local functions rather than global (better programming).
				Abstracted summary text to seperate function for future changes
				Lots of old debug comments and code removed
v0.0.2	31/01/09	JXW	Remove string appending and create summary table to report from
v0.0.1	14/01/09	JXW	First working version, nasty code


Details
	Order reported in:
	Melee +dam (abil, auto)
	Ranged +dam (abil, auto)
	Melee -dam taken (armour)
	Armour pen

	Chance to be crit
	Dodge
	Parry
	Block
	All -dam (tough)
	Hits

	Mag +dam
	Heal +heal
	Disrupt %

	Spirit res
	Corp res
	Elem res

	?? Agility ??
	Unknown

]]

WARRatingBuster = {}
WARRatingBuster.version = "v0.2.5"


local trb_Indices = {
	"MeleeAbilityDPS",
	"RangedAbilityDPS",
	"MeleeDPSReduction",
	"ArmourPen",
	"ChanceToBeCrit",
	"Dodge",
	"Parry",
	"Block",
	"AllDPSReduction",
	"HitPoints",
	"MagicDPS",
	"HealPS",
	"SpellDisrupt",
	"SpiRes",
	"ElemRes",
	"CorpRes",
	"Agility",
	"Unknown"
}

local trb_SummaryStrings = { MeleeAbilityDPS="Melee Ability/auto +%0.1f / +%0.1f DPS\n",
			RangedAbilityDPS="Ranged Ability/auto +%0.1f / +%0.1f DPS\n",
			MeleeDPSReduction="Melee damage -%0.1f%%\n",
			ArmourPen="Armour Penetration %0.1f%%\n",
			ChanceToBeCrit="Be crit %0.1f%%\n",
			Dodge="Dodge %0.1f%%\n",
			Parry="Parry %0.1f%%\n",
			Block="Block %0.1f%%\n",
			AllDPSReduction="Reduce %0.1f DPS taken\n",
			HitPoints="Hit Points +%d\n",
			MagicDPS="Magical attacks +%0.1f DPS\n",
			HealPS="Heal +%0.1f per sec\n",
			SpellDisrupt="Spell disrupt +%0.1f%%\n",
			SpiRes="Spiritual Resist %0.1f%%\n",
			ElemRes="Elemental Resist %0.1f%%\n",
			CorpRes="Corporeal Resist %0.1f%%\n",
			Agility="Agility +%d\n",
			Unknown="?? %s ??\n"
		}	

local trb_SummaryTalisStrings = { MeleeAbilityDPS="Melee Ability/auto +%0.1f / +%0.1f DPS (base: %0.1f / %0.1f)\n",
			RangedAbilityDPS="Ranged Ability/auto +%0.1f / +%0.1f (base: %0.1f / %0.1f) DPS\n",
			MeleeDPSReduction="Melee damage -%0.1f%% (base: -%0.1f%%)\n",
			ArmourPen="Armour Penetration %0.1f%% (base: %0.1f%%)\n",
			ChanceToBeCrit="Be crit %0.1f%% (base: %0.1f%%)\n",
			Dodge="Dodge %0.1f%% (base: %0.1f%%)\n",
			Parry="Parry %0.1f%% (base: %0.1f%%)\n",
			Block="Block %0.1f%% (base: %0.1f%%)\n",
			AllDPSReduction="Reduce %0.1f (base: %0.1f) DPS taken\n",
			HitPoints="Hit Points +%d (base: +%d)\n",
			MagicDPS="Magical attacks +%0.1f (base: %0.1f) DPS\n",
			HealPS="Heal +%0.1f (base: %0.1f) per sec\n",
			SpellDisrupt="Spell disrupt +%0.1f%% (base: %0.1f%%)\n",
			SpiRes="Spiritual Resist %0.1f%% (base: %0.1f%%)\n",
			ElemRes="Elemental Resist %0.1f%% (base: %0.1f%%)\n",
			CorpRes="Corporeal Resist %0.1f%% (base: %0.1f%%)\n",
			Agility="Agility +%d (base: %d)\n",
			Unknown="?? %s ??\n"
		}



---------------------------
-- local variables --------
---------------------------

-- event hook functions
local OldCreateItemTooltip
local OldAddExtraWindow

local l_strWindowNameBase = "RatingToolTip";

-- array of windows to create, labels inside are name+"Info"
local l_WindowArr =
{ 
	l_strWindowNameBase .. "1",
	l_strWindowNameBase .. "2",
	l_strWindowNameBase .. "3",
	l_strWindowNameBase .. "4",
	l_strWindowNameBase .. "5"
}

local l_CurrentWindowNumber = 1;

-- to hold user-defined algorithms
local l_alg ={}
--[[{
	{algname="tank", alg="s=1,t=1,ws=0.8,i=0.8,w=2,arm=0.01,spi=0.01,cor=0.01,ele=0.01,brt=0.05,rapen=5,ihat=5,dhat=-5,bcrit=5,melp=0.8,mcrit=2", vis=1},
	{algname="total", alg="s=1,t=1,ws=1,i=1,w=1,bs=1,int=1,wp=1", vis=1},
	{algname="pveheal", alg="wp=1,dhat=1,hcrit=5,heap=1", vis=1},
	{algname="rvrheal", alg="wp=1,hcrit=5,heap=0.8,t=1,w=2,spi=0.01,cor=0.01,ele=0.01,arm=0.05,rapen=5,bcrit=5", vis=1}
}
--]]

---------------------------
-- local functions --------
---------------------------

local function Message(str)
	EA_ChatWindow.Print(towstring("[WARRatingBuster]: ".. str))
end


-- functions for storing / finding / keeping user algorithms
local function findAlg(findAlgName)
	local toReturn = 0
	if(l_alg ~= nil) then
		for i,v in pairs (l_alg) do
			if (v.algname) then
				-- Message("Checking "..v.algname.." for "..findAlgName)
				if (v.algname:lower() ==findAlgName:lower() ) then
					toReturn = i
					break
				end
			end
		end	
	end	
	return toReturn
end

local function newAlg(newAlgName, newAlg)
	-- Message("Adding "..newAlgName)
	local l_new = {algname = "", alg = "", vis=1}
	l_new.algname = newAlgName
	l_new.alg = newAlg
	if(l_alg ~= nil) then
		table.insert(l_alg, l_new)
	else
		l_alg = {}
		l_alg[1] = l_new
	end
	return findAlg(newAlgName)
end

local function removeAlg(delAlgName)
	local iPos = findAlg(delAlgName)
	if(iPos > 0) then
		table.remove(l_alg, iPos)
	end	
end

local function setAlg(setAlgName, setAlg)
	local iPos = findAlg(setAlgName)
	if(iPos == 0) then
		iPos = newAlg(setAlgName, setAlg)
	else
		l_alg[iPos].alg = setAlg
	end	
end

local function toggleAlg(sAlgName)
	local iPos = findAlg(sAlgName)
	if(iPos > 0) then
		if(l_alg[iPos].vis == 1) then
			l_alg[iPos].vis = 0
		else
			l_alg[iPos].vis = 1
		end
	end
end

-- functions for working out item stats
-- TODO: find +Armour Pen: l_table.PlusArmourPen
--		18805 - -10% crit taken damage, how to work into table etc

local function GetBonusFor(l_type, l_val, l_table, l_idtype, l_idduration)
	local l_level = GameData.Player.level
	local l_returnTableCol, l_returnVal
	if(l_idtype == nil) then
		l_idtype = 0
	end
	if(l_idduration == nil) then
		l_idduration = 0
	end	

	if l_type == 1 then											-- Strength
		l_table.MeleeAbilityDPS = l_table.MeleeAbilityDPS + (l_val/5)
		l_table.Strength = l_table.Strength + l_val

	elseif l_type == 2 then											-- "Agility"
		l_table.Agility = l_table.Agility + l_val

	elseif l_type == 3 then											-- Willpower, see also 15618 (hybrid pot)
		l_table.HealPS = l_table.HealPS + (l_val/5)
		l_table.SpellDisrupt = l_table.SpellDisrupt + (l_val / (l_level * 7.5 + 50) * .075 * 100)
		l_table.Willpower = l_table.Willpower + l_val

	elseif l_type == 4 then											-- Toughness
		l_table.AllDPSReduction = l_table.AllDPSReduction + (l_val/5)
		l_table.Toughness = l_table.Toughness + l_val

	elseif l_type == 5 then											-- Wounds
		l_table.HitPoints = l_table.HitPoints + (l_val*10)
		l_table.Wounds = l_table.Wounds + l_val

	elseif l_type == 6 then											-- Initiative - see also 84
		local l_baseInit = GameData.Player.Stats[6].baseValue
		local l_withthisInit = l_baseInit + l_val
		local l_critBase = (l_level*7.5+50)/l_baseInit*10
		local l_critThis = (l_level*7.5+50)/l_withthisInit*10
		l_table.ChanceToBeCrit = l_table.ChanceToBeCrit + l_critThis - l_critBase
		l_table.Dodge = l_table.Dodge + ((l_val / (l_level * 7.5 + 50)) * 7.5)
		l_table.Initiative = l_table.Initiative + l_val

	elseif l_type == 7 then											-- Weapon Skill
		l_table.Parry = l_table.Parry + ((l_val / (l_level * 7.5 + 50)) * 7.5)
		l_table.ArmourPen = l_table.ArmourPen + ((l_val / (l_level * 7.5 + 50)) * 25)
		l_table.WeaponSkill = l_table.WeaponSkill + l_val

	elseif l_type == 8 then											-- Ballistic Skill
		l_table.RangedAbilityDPS = l_table.RangedAbilityDPS + (l_val/5)
		l_table.BallisticSkill = l_table.BallisticSkill + l_val

	elseif l_type == 9 then											-- Intelligence
		l_table.MagicDPS = l_table.MagicDPS + (l_val/5)
		l_table.Intelligence = l_table.Intelligence + l_val

	elseif l_type == 10 then										-- Block
		l_table.Block = l_table.Block + ((l_val / (l_level * 7.5 + 50)) * 20)
		l_table.BlockRating = l_table.BlockRating + l_val

	elseif l_type == 11 then										-- Parry
		l_table.Parry = l_table.Parry + l_val
		l_table.PlusParry = l_table.PlusParry + l_val

	elseif l_type == 12 then										-- Evade
		l_table.Dodge = l_table.Dodge + l_val
		l_table.PlusDodge = l_table.PlusDodge + l_val

	elseif l_type == 13 then										-- Disrupt
		l_table.SpellDisrupt = l_table.SpellDisrupt + l_val
		l_table.PlusDisrupt = l_table.PlusDisrupt + l_val

	elseif l_type == 14 then										-- Spirit Res
		l_table.SpiRes = l_table.SpiRes + (l_val / (l_level * 16.8) * .4 * 100)
		l_table.SpiResTot = l_table.SpiResTot + l_val

	elseif l_type == 15 then										-- Elem Res
		l_table.ElemRes = l_table.ElemRes + (l_val / (l_level * 16.8) * .4 * 100)
		l_table.ElemResTot = l_table.ElemResTot + l_val
		
	elseif l_type == 16 then										-- Corp Res
		l_table.CorpRes = l_table.CorpRes + (l_val / (l_level * 16.8) * .4 * 100)
		l_table.CorpResTot = l_table.CorpResTot + l_val

	elseif l_type == 26 then										-- +armour
		l_table.MeleeDPSReduction = l_table.MeleeDPSReduction + (l_val / (l_level * 44) * 40)
		l_table.Armour = l_table.Armour + l_val

	elseif l_type == 28 then										-- +block%
		l_table.Block = l_table.Block + l_val
		l_table.PlusBlock = l_table.PlusBlock + l_val

	elseif l_type == 29 then										-- +parry%
		l_table.Parry = l_table.Parry + l_val
		l_table.PlusParry = l_table.PlusParry + l_val
		
	elseif l_type == 30 then										-- +dodge %
		l_table.Dodge = l_table.Dodge + l_val
		l_table.PlusDodge = l_table.PlusDodge + l_val

	elseif l_type == 31 then										-- +disrupt%
		l_table.SpellDisrupt = l_table.SpellDisrupt + l_val
		l_table.PlusDisrupt = l_table.PlusDisrupt + l_val

	elseif l_type == 32 then										-- +AP per sec
		l_table.APPerSec = l_table.APPerSec + l_val
		
	elseif l_type == 33 then										-- +morale per sec
		l_table.MoralePerSec = l_table.MoralePerSec + l_val
		
	elseif l_type == 67 then										-- threat increase
		l_table.HateIncrease = l_table.HateIncrease + l_val
		
	elseif l_type == 68 then										-- threat reduction
		l_table.HateDecrease = l_table.HateDecrease + l_val
		
	elseif l_type == 76 then										-- +melee crit chance
		l_table.MeleeCrit = l_table.MeleeCrit + l_val
		
	elseif l_type == 77 then										-- +ranged crit chance
		l_table.RangeCrit = l_table.RangeCrit + l_val
		
	elseif l_type == 78 then										-- +magic crit chance
		l_table.MagicCrit = l_table.MagicCrit + l_val
		
	elseif l_type == 79 then										-- +hit points / 4 secs
		l_table.HPPerFourSec = l_table.HPPerFourSec + l_val
		
	elseif l_type == 80 then										-- +melee power -- like strength without the extra reductions
		l_table.MeleeAbilityDPS = l_table.MeleeAbilityDPS + (l_val/5)
		l_table.MeleePower = l_table.MeleePower + l_val

	elseif l_type == 81 then										-- +ranged power -- like Ballistic without extra reductions
		l_table.RangedAbilityDPS = l_table.RangedAbilityDPS + (l_val/5)
		l_table.RangePower = l_table.RangePower + l_val

	elseif l_type == 82 then										-- +magic power -- like Intelligence without extra reductions
		l_table.MagicDPS = l_table.MagicDPS + (l_val/5)
		l_table.MagicPower = l_table.MagicPower + l_val

	elseif l_type == 83 then										-- +reduced armour penetration%
		l_table.ReducedArmourPen = l_table.ReducedArmourPen + l_val
		
	elseif l_type == 84 then										-- reduction chance in critically hit, see also Initiative (6)
		l_table.ChanceToBeCrit = l_table.ChanceToBeCrit - l_val
		l_table.ReduceBeCrit = l_table.ReduceBeCrit + l_val
		
	elseif l_type == 86 then										-- reduction in chance to be parried
		l_table.ReducedBeParried = l_table.ReducedBeParried + l_val
		
	elseif l_type == 87 then										-- reduction in chance to be evaded
		l_table.ReducedBeDodged = l_table.ReducedBeDodged + l_val
		
	elseif l_type == 88 then										-- reduced chance to be disrupted
		l_table.ReducedBeDisrupted = l_table.ReducedBeDisrupted + l_val
		
	elseif l_type == 89 then										-- + healing crit
		l_table.HealCrit = l_table.HealCrit + l_val
		
	elseif l_type == 94 then										-- +healing power -- same as willpower, without the disrupt
		l_table.HealPS = l_table.HealPS + (l_val/5)
		l_table.HealPower = l_table.HealPower + l_val

-----------------
---- POTIONS ----
-----------------

	elseif l_type == 7526 then										-- eversource wine lvl 21, +60 elem resist, 60 mins
		l_table.ElemRes = l_table.ElemRes + (60 / (l_level * 16.8) * .4 * 100)

	elseif l_type == 7898 then										-- strength pot lvl 25, +48, 10 mins
														-- also a lvl 20 +40 strength pot has same effect ID
		l_table.MeleeAbilityDPS = l_table.MeleeAbilityDPS + (48/5)
		l_table.Unknown = l_table.Unknown..string.format("Strength pot (%d/%d/%d/%d)", l_type, l_val, l_idtype, l_idduration)

	elseif l_type == 7913 then										-- toughness pot lvl 25, +48, 10 mins
		l_table.AllDPSReduction = l_table.AllDPSReduction + (48/5)

	elseif l_type == 7917 then										-- spirit resist pot lvl 10, +75 resist, 5 mins
		l_table.SpiRes = l_table.SpiRes + (75 / (l_level * 16.8) * .4 * 100)

	elseif l_type == 7920 then										-- spirit resist pot lvl 25, +151 resist, 30 mins
		l_table.SpiRes = l_table.SpiRes + (151 / (l_level * 16.8) * .4 * 100)

	elseif l_type == 7928 then										-- corp resist pot lvl 40, +277 resist, 10 mins
		l_table.CorpRes = l_table.CorpRes + (277 / (l_level * 16.8) * .4 * 100)

	elseif l_type == 7934 then										-- armour pot lvl 25, +396 armour, 15 mins
		l_table.MeleeDPSReduction = l_table.MeleeDPSReduction + (396 / (l_level * 44) * 40)

	elseif l_type == 7935 then										-- armour pot lvl 25, +396 armour, 30 mins
														-- also a lvl 15 pot, +264, 30 mins
		l_table.MeleeDPSReduction = l_table.MeleeDPSReduction + (396 / (l_level * 44) * 40)
		l_table.Unknown = l_table.Unknown..string.format("Armour pot (%d/%d/%d/%d)", l_type, l_val, l_idtype, l_idduration)

-----------------
---- HYBRIDS ----
-----------------
	elseif l_type == 15616 then										-- hybrid pot, +60 Toughness, +5 melee crit
		l_table.Toughness = l_table.Toughness + 60
		l_table.AllDPSReduction = l_table.AllDPSReduction + (60/5)
		l_table.MeleeCrit = l_table.MeleeCrit + 5

	elseif l_type == 15617 then										-- hybrid pot, +60 Strength, +50 melee Power
		l_table.Strength = l_table.Strength + 60
		l_table.MeleeAbilityDPS = l_table.MeleeAbilityDPS + (110/5)

	elseif l_type == 15618 then										-- hybrid pot, +60 Willpower, +50 Healing Power (willpower heal, no disrupt)
		l_table.Willpower = l_table.Willpower + 60
		l_table.HealPS = l_table.HealPS + (110/5)
		l_table.SpellDisrupt = l_table.SpellDisrupt + (60 / (l_level * 7.5 + 50) * .075 * 100)

	elseif l_type == 15619 then										-- hybrid pot, +60 Strength, +50 Healing Power (willpower heal, no disrupt)
		l_table.Strength = l_table.Strength + 60
		l_table.HealPS = l_table.HealPS + (50/5)
		l_table.MeleeAbilityDPS = l_table.MeleeAbilityDPS + (60/5)

	elseif l_type == 15620 then										-- hybrid pot, +60 Intelligence, +5% magic crit
		l_table.Intelligence = l_table.Intelligence + 60
		l_table.MagicDPS = l_table.MagicDPS + (60/5)
		l_table.MagicCrit = l_table.MagicCrit + 5

	elseif l_type == 15621 then										-- hybrid pot, +60 Ballistic, +5% ranged crit
		l_table.BallisticSkill = l_table.BallisticSkill + 60 
		l_table.RangedAbilityDPS = l_table.RangedAbilityDPS + (60/5)
		l_table.RangeCrit = l_table.RangeCrit + 5

	elseif l_type == 15622 then										-- hybrid pot, +60 Wounds, +50 Melee Power (strength dps, no block etc)
		l_table.MeleeAbilityDPS = l_table.MeleeAbilityDPS + (50/5)
		l_table.HitPoints = l_table.HitPoints + 600
		l_table.Wounds = l_table.Wounds + 60

	elseif l_type == 15623 then										-- hybrid pot, +60 Wounds, +5% melee crit
		l_table.HitPoints = l_table.HitPoints + 600
		l_table.MeleeCrit = l_table.MeleeCrit + 5
		l_table.Wounds = l_table.Wounds + 60

	elseif l_type == 15624 then										-- hybrid pot, +60 Wounds, +5% magic crit
		l_table.HitPoints = l_table.HitPoints + 600
		l_table.MagicCrit = l_table.MagicCrit + 5
		l_table.Wounds = l_table.Wounds + 60

	elseif l_type == 15625 then										-- hybrid pot, +60 Wounds, +5% ranged crit
		l_table.HitPoints = l_table.HitPoints + 600
		l_table.RangeCrit = l_table.RangeCrit + 5
		l_table.Wounds = l_table.Wounds + 60

	elseif l_type == 15626 then										-- hybrid pot, +60 Wounds, +50 Healing Power (willpower heal, no disrupt)
		l_table.HealPS = l_table.HealPS + (50/5)
		l_table.HitPoints = l_table.HitPoints + 600
		l_table.Wounds = l_table.Wounds + 60

	elseif l_type == 15627 then										-- hybrid pot, +60 Wounds, +50 Strength
		l_table.Strength = l_table.Strength + 50
		l_table.MeleeAbilityDPS = l_table.MeleeAbilityDPS + (50/5)
		l_table.HitPoints = l_table.HitPoints + 600
		l_table.Wounds = l_table.Wounds + 60

	-- extra new ones	
	elseif l_type == 18801 then										-- hybrid pot, +180 all resists
		l_table.SpiRes = l_table.SpiRes + (180 / (l_level * 16.8) * .4 * 100)
		l_table.ElemRes = l_table.ElemRes + (180 / (l_level * 16.8) * .4 * 100)
		l_table.CorpRes = l_table.CorpRes + (180 / (l_level * 16.8) * .4 * 100)
		l_table.SpiResTot = l_table.SpiResTot + 180
		l_table.ElemResTot = l_table.ElemResTot + 180
		l_table.CorpResTot = l_table.CorpResTot + 180

	elseif l_type == 18803 then										-- hybrid pot, +60 wounds, +60 initiative
		l_table.HitPoints = l_table.HitPoints + 600
		l_table.Wounds = l_table.Wounds + 60

		local l_baseInit = GameData.Player.Stats[6].baseValue
		local l_withthisInit = l_baseInit + 60
		local l_critBase = (l_level*7.5+50)/l_baseInit*10
		local l_critThis = (l_level*7.5+50)/l_withthisInit*10
		l_table.ChanceToBeCrit = l_table.ChanceToBeCrit + l_critThis - l_critBase
		l_table.Dodge = l_table.Dodge + ((60 / (l_level * 7.5 + 50)) * 7.5)
		l_table.Initiative = l_table.Initiative + 60

	elseif l_type == 18805 then										-- hybrid pot, +20 health regen, -10% crit damage
		l_table.HPPerFourSec = l_table.HPPerFourSec + 20
		
	elseif l_val > 0 then
		l_table.Unknown = l_table.Unknown..string.format("(%d/%d/%d/%d)", l_type, l_val, l_idtype, l_idduration)
	end
	
	--d("type: "..l_type)
end

local function StatRatings(itemData)
	local t_rbSummary = {	MeleeAbilityDPS=0, 
				RangedAbilityDPS=0,
				MeleeDPSReduction=0,
				ArmourPen=0,
				ChanceToBeCrit=0,
				Dodge=0,
				Parry=0,
				Block=0,
				AllDPSReduction=0,
				HitPoints=0,
				MagicDPS=0,
				HealPS=0,
				SpellDisrupt=0,
				SpiRes=0,
				ElemRes=0,
				CorpRes=0,
				Agility=0,
				Unknown = "",
				
				Strength=0,
				WeaponSkill=0,
				BallisticSkill=0,
				Intelligence=0,
				Toughness=0,
				Wounds=0,
				Initiative=0,
				Willpower=0,
				
				SpiResTot=0,
				CorpResTot=0,
				ElemResTot=0,
				
				Armour=0,
				BlockRating=0,
				ReducedBeBlocked=0,
				ReducedBeParried=0,
				ReducedBeDodged=0,
				ReducedBeDisrupted=0,
				ReducedArmourPen=0,
				
				PlusBlock=0,
				PlusParry=0,
				PlusDodge=0,
				PlusDisrupt=0,
				PlusArmourPen=0,
				
				APPerSec=0,
				MoralePerSec=0,
				HPPerFourSec=0,
				HateIncrease=0,
				HateDecrease=0,
				
				MeleeCrit=0,
				RangeCrit=0,
				MagicCrit=0,
				HealCrit=0,
				ReduceBeCrit=0,
				
				MeleePower=0,
				RangePower=0,
				MagicPower=0,
				HealPower=0			
				
			}

	
	local t_rbTalis = {	MeleeAbilityDPS=0, 
				RangedAbilityDPS=0,
				MeleeDPSReduction=0,
				ArmourPen=0,
				ChanceToBeCrit=0,
				Dodge=0,
				Parry=0,
				Block=0,
				AllDPSReduction=0,
				HitPoints=0,
				MagicDPS=0,
				HealPS=0,
				SpellDisrupt=0,
				SpiRes=0,
				ElemRes=0,
				CorpRes=0,
				Agility=0,
				Unknown = "",
				
				Strength=0,
				WeaponSkill=0,
				BallisticSkill=0,
				Intelligence=0,
				Toughness=0,
				Wounds=0,
				Initiative=0,
				Willpower=0,
				
				SpiResTot=0,
				CorpResTot=0,
				ElemResTot=0,
				
				Armour=0,
				BlockRating=0,
				ReducedBeBlocked=0,
				ReducedBeParried=0,
				ReducedBeDodged=0,
				ReducedBeDisrupted=0,
				ReducedArmourPen=0,
				
				PlusBlock=0,
				PlusParry=0,
				PlusDodge=0,
				PlusDisrupt=0,
				PlusArmourPen=0,
				
				APPerSec=0,
				MoralePerSec=0,
				HPPerFourSec=0,
				HateIncrease=0,
				HateDecrease=0,
				
				MeleeCrit=0,
				RangeCrit=0,
				MagicCrit=0,
				HealCrit=0,
				ReduceBeCrit=0,
				
				MeleePower=0,
				RangePower=0,
				MagicPower=0,
				HealPower=0			
				
			}

	
	local ii = 0	-- number of lines

	for l_bonNum, l_bonus in pairs(itemData.bonus) do
		local l_value = l_bonus.value
		local l_stattype = l_bonus.reference
		local l_idType = l_bonus.type
		local l_idDuration = l_bonus.duration
		
		GetBonusFor(l_stattype, l_value, t_rbSummary, l_idType, l_idDuration)

	end
	-- talisman slots
	for l_SlotNum, l_SlotData in pairs(itemData.enhSlot) do
		for l_bonNum, l_bonus in pairs(l_SlotData.bonus) do
			--GetBonusFor(l_bonus.reference, l_bonus.value, t_rbSummary, l_bonus.type, l_bonus.duration)
			GetBonusFor(l_bonus.reference, l_bonus.value, t_rbTalis, l_bonus.type, l_bonus.duration)
		end
	end
	
	local l_level = GameData.Player.level

	if itemData.blockRating > 0 then
		t_rbSummary.Block = t_rbSummary.Block + ((itemData.blockRating / (l_level * 7.5 + 50)) * 20)
		t_rbSummary.BlockRating = t_rbSummary.BlockRating + itemData.blockRating
	end
	
	if itemData.armor > 0 then
		t_rbSummary.MeleeDPSReduction = t_rbSummary.MeleeDPSReduction + (itemData.armor / (l_level * 44) * 40)
		t_rbSummary.Armour = t_rbSummary.Armour + itemData.armor
	end
	for i,v in pairs(t_rbSummary) do
		if (t_rbSummary[i] ~= 0 and t_rbSummary[i] ~= "") or (t_rbTalis[i] ~= 0 and t_rbTalis[i] ~= "") then
			ii=ii+1
		end	
	end
	return t_rbSummary, t_rbTalis, ii
end

local function GetSummaryText(itemData)
	local l_bonus = towstring("")
	local nCount = 0, v, x, y
	if(itemData ~= nil) then
		local tBase, tTalisSlotted
		tBase, tTalisSlotted, nCount = StatRatings(itemData)
		if nCount > 0 then
			-- Message(l_bonus)
			-- l_bonus = towstring("Rating Buster\n") .. itemData.name .. towstring("\n")
			-- nCount = nCount + 2
			nCount = 0
			for i,l_keyname in ipairs(trb_Indices) do
				v = tBase[l_keyname]
				x = tTalisSlotted[l_keyname]
				y = 0
				if (v ~=0 and v ~= "") or (x ~=0 and x ~= "") then
					-- because having base AND talis seperate now, add them up for total
					if (v ~= 0 and v ~= "") then
						y = y + v
					end
					if (x ~= 0 and x ~= "") then
						y = y + x
					end	
					if (l_keyname == "MeleeAbilityDPS") or (l_keyname == "RangedAbilityDPS") then
						if(x ~= 0) then
							l_bonus = l_bonus..towstring(string.format(trb_SummaryTalisStrings[l_keyname], y, y/2, v, v/2))
						else
							l_bonus = l_bonus..towstring(string.format(trb_SummaryStrings[l_keyname], y, y/2))
						end	
						--l_bonus = l_bonus..towstring(string.format(trb_SummaryStrings[l_keyname], v, v/2))
						-- add in extra damage, increase text row count by same amount
						-- this can come from the item's speed or from equipped speed if not a weapon
						if itemData.speed > 0.1 then
							-- this item is a weapon
							if(x ~= 0) then
								l_bonus = l_bonus..towstring(string.format(" (+%.1f / +%.1f damage, base %.1f/%.1f)\n", y*itemData.speed, y/2*itemData.speed,v*itemData.speed, v/2*itemData.speed))
							else
								l_bonus = l_bonus..towstring(string.format(" (+%.1f / +%.1f damage)\n", y*itemData.speed, y/2*itemData.speed))
							end
							--l_bonus = l_bonus..towstring(string.format(" (+%.1f / +%.1f damage)\n", (v*itemData.speed), v/2*itemData.speed))
							nCount = nCount + 1
						end
					else
						if v~=0 and x ~=0 then
							l_bonus = l_bonus..towstring(string.format(trb_SummaryTalisStrings[l_keyname], y, v))
						else
							l_bonus = l_bonus..towstring(string.format(trb_SummaryStrings[l_keyname], y))
						end	
					end
					nCount = nCount + 1
				end	
			end
		end
	end
	return l_bonus, nCount
end

local function explode(d,p)
	local t, ll
	t={}
	ll=0
	if(#p == 1) then return p end
	while true do
		l=string.find(p,d,ll+1,true) -- find the next d in the string
		if l~=nil then -- if "not not" found then..
			table.insert(t, string.sub(p,ll,l-1)) -- Save it in our array.
			ll=l+1 -- save just after where we found it for searching next time.
		else
			table.insert(t, string.sub(p,ll)) -- Save what's left in our array.
			break -- Break at end, as it should be, according to the lua manual.
		end
	end
	return t
end


local function CalcAlgorithm (strAlgToUse, tblStats)
	local tt=0
	local l_explanation = ""
	local tblAlg = explode(",", strAlgToUse)
	-- now table of stat=val strings
	for i, v in pairs(tblAlg) do
		local fields = explode("=", v)
		local stat = tostring(fields[1]):lower()
		local ratio = tonumber(tostring(fields[2]):lower())
		if(ratio == nil) then
			Message("Nil ratio [" .. v .. "]")
		else	
			l_explanation = l_explanation .. stat .."[" .. ratio .. " x "
			if(stat=="s") then
				tt = tt + (tblStats.Strength * ratio)
				l_explanation = l_explanation .. tblStats.Strength
			elseif(stat=="ws") then	
				tt = tt + (tblStats.WeaponSkill * ratio)
				l_explanation = l_explanation .. tblStats.WeaponSkill
			elseif(stat=="bs") then	
				tt = tt + (tblStats.BallisticSkill * ratio)
				l_explanation = l_explanation .. tblStats.BallisticSkill
			elseif(stat=="int") then	
				tt = tt + (tblStats.Intelligence * ratio)
				l_explanation = l_explanation .. tblStats.Intelligence
			elseif(stat=="t") then	
				tt = tt + (tblStats.Toughness * ratio)
				l_explanation = l_explanation .. tblStats.Toughness
			elseif(stat=="w") then	
				tt = tt + (tblStats.Wounds * ratio)
				l_explanation = l_explanation .. tblStats.Wounds
			elseif(stat=="i") then	
				tt = tt + (tblStats.Initiative * ratio)
				l_explanation = l_explanation .. tblStats.Initiative
			elseif(stat=="wp") then	
				tt = tt + (tblStats.Willpower * ratio)
				l_explanation = l_explanation .. tblStats.Willpower
			elseif(stat=="spi") then
				tt = tt + (tblStats.SpiResTot * ratio)
				l_explanation = l_explanation .. tblStats.SpiResTot
			elseif(stat=="cor") then
				tt = tt + (tblStats.CorpResTot * ratio)
				l_explanation = l_explanation .. tblStats.CorpResTot
			elseif(stat=="ele") then
				tt = tt + (tblStats.ElemResTot * ratio)
				l_explanation = l_explanation .. tblStats.ElemResTot
			elseif(stat=="arm") then	
				tt = tt + (tblStats.Armour * ratio)
				l_explanation = l_explanation .. tblStats.Armour
			elseif(stat=="brt") then	
				tt = tt + (tblStats.BlockRating * ratio)
				l_explanation = l_explanation .. tblStats.BlockRating
			elseif(stat=="bblk") then	
				tt = tt + (tblStats.ReducedBeBlocked * ratio)
				l_explanation = l_explanation .. tblStats.ReducedBeBlocked
			elseif(stat=="bpar") then	
				tt = tt + (tblStats.ReducedBeParried * ratio)
				l_explanation = l_explanation .. tblStats.ReducedBeParried
			elseif(stat=="bdod") then	
				tt = tt + (tblStats.ReducedBeDodged * ratio)
				l_explanation = l_explanation .. tblStats.ReducedBeDodged
			elseif(stat=="bdis") then	
				tt = tt + (tblStats.ReducedBeDisrupted * ratio)
				l_explanation = l_explanation .. tblStats.ReducedBeDisrupted
			elseif(stat=="rapen") then	
				tt = tt + (tblStats.ReducedArmourPen * ratio)
				l_explanation = l_explanation .. tblStats.ReducedArmourPen
			elseif(stat=="aps") then	
				tt = tt + (tblStats.APPerSec * ratio)
				l_explanation = l_explanation .. tblStats.APPerSec
			elseif(stat=="mps") then	
				tt = tt + (tblStats.MoralePerSec * ratio)
				l_explanation = l_explanation .. tblStats.MoralePerSec
			elseif(stat=="hps") then	
				tt = tt + (tblStats.HPPerFourSec * ratio)
				l_explanation = l_explanation .. tblStats.HPPerFourSec
			elseif(stat=="ihat") then	
				tt = tt + (tblStats.HateIncrease * ratio)
				l_explanation = l_explanation .. tblStats.HateIncrease
			elseif(stat=="dhat") then	
				tt = tt + (tblStats.HateDecrease * ratio)
				l_explanation = l_explanation .. tblStats.HateDecrease
			elseif(stat=="melc") then	
				tt = tt + (tblStats.MeleeCrit * ratio)
				l_explanation = l_explanation .. tblStats.MeleeCrit
			elseif(stat=="ranc") then	
				tt = tt + (tblStats.RangeCrit * ratio)
				l_explanation = l_explanation .. tblStats.RangeCrit
			elseif(stat=="magc") then	
				tt = tt + (tblStats.MagicCrit * ratio)
				l_explanation = l_explanation .. tblStats.MagicCrit
			elseif(stat=="heac") then	
				tt = tt + (tblStats.HealCrit * ratio)
				l_explanation = l_explanation .. tblStats.HealCrit
			elseif(stat=="bcrit") then	
				tt = tt + (tblStats.ReduceBeCrit * ratio)
				l_explanation = l_explanation .. tblStats.ReduceBeCrit
			elseif(stat=="melp") then	
				tt = tt + (tblStats.MeleePower * ratio)
				l_explanation = l_explanation .. tblStats.MeleePower
			elseif(stat=="ranp") then	
				tt = tt + (tblStats.RangePower * ratio)
				l_explanation = l_explanation .. tblStats.RangePower
			elseif(stat=="magp") then	
				tt = tt + (tblStats.MagicPower * ratio)
				l_explanation = l_explanation .. tblStats.MagicPower
			elseif(stat=="heap") then	
				tt = tt + (tblStats.HealPower * ratio)
				l_explanation = l_explanation .. tblStats.HealPower

			elseif(stat=="pblk") then	
				tt = tt + (tblStats.PlusBlock * ratio)
				l_explanation = l_explanation .. tblStats.PlusBlock
			elseif(stat=="ppar") then	
				tt = tt + (tblStats.PlusParry * ratio)
				l_explanation = l_explanation .. tblStats.PlusParry
			elseif(stat=="pdod") then	
				tt = tt + (tblStats.PlusDodge * ratio)
				l_explanation = l_explanation .. tblStats.PlusDodge
			elseif(stat=="pdis") then	
				tt = tt + (tblStats.PlusDisrupt * ratio)
				l_explanation = l_explanation .. tblStats.PlusDisrupt
			elseif(stat=="papen") then	
				tt = tt + (tblStats.PlusArmourPen * ratio)
				l_explanation = l_explanation .. tblStats.PlusArmourPen
			end	
			l_explanation = l_explanation .. "] "
		end	
	end
	return tt, l_explanation
end

local function AddStatWindow (windowName, windowToAnchorTo, itemData, isMainTooltip)
    
	local nCount = 0
	local l_bonus = towstring("")
	local FONT_HEIGHT = 20
	local TOP_MARGIN = 10
	local nRatingWindowHeight = 0
	local nWidthOffset = 0
	local sAnchorTo = ""
	local sAnchorRel = ""
	local sAnchorWin = windowToAnchorTo
	local l_debugexplanation = ""
	local l_statexp = ""
	
	if(itemData ~= nil) then
		l_bonus, nCount = GetSummaryText(itemData)
		if nCount > 0 then
			-- add in the extra algorithms
			local l_vis = 0
			if(l_alg ~= nil and #l_alg > 0) then
				-- nCount = nCount + 1
				-- l_bonus = l_bonus..towstring("\n")
				for i,v in pairs(l_alg) do
					if(v.vis == 1) then
						l_vis = l_vis + 1
						l_BaseTab, l_TalisTab, _ = StatRatings(itemData)
						l_algtot, l_statexp = CalcAlgorithm(v.alg, l_BaseTab)
						l_talisTot, l_talisExp = CalcAlgorithm(v.alg, l_TalisTab)
						if l_talisTot > 0 then
							l_bonus = l_bonus..towstring(string.format("\n%s: %.2f (base: %.2f)", v.algname, l_algtot + l_talisTot, l_algtot))
							l_debugexplanation = l_debugexplanation .. l_statexp .. " tals: " .. l_talisExp
						else
							l_bonus = l_bonus..towstring(string.format("\n%s: %.2f", v.algname, l_algtot))
							l_debugexplanation = l_debugexplanation .. l_statexp
						end	
					end	
				end
				
			end
			-- d("WRB explanation: " .. l_debugexplanation)
			if(l_vis >0) then
				l_vis = l_vis + 1
			end
			
			nCount = nCount + l_vis
			nRatingWindowHeight = ( (nCount) * FONT_HEIGHT) + TOP_MARGIN
			
			local index = 1
			while( Tooltips.curExtraWindows[index] ~= nil ) do
				index = index + 1
			end

			WindowSetShowing( windowName, true )
			Tooltips.curExtraWindows[index] = { name = windowName, data = l_bonus };

			-- Anchor the additional tooltip windows differently, depending on which side
			-- of the cursor the tooltip is placed
			local tooltipX, tooltipY        = WindowGetScreenPosition (Tooltips.curTooltipWindow);
			local mouseoverX, mouseoverY    = WindowGetScreenPosition (Tooltips.curMouseOverWindow);

			local wstrWindowName    = StringToWString (windowName);
			local wstrAnchorName    = StringToWString (windowToAnchorTo);

			WindowClearAnchors (windowName);
			local w, h = WindowGetDimensions( windowToAnchorTo )    
			WindowSetDimensions(windowName, 300, nRatingWindowHeight)
			local screenWidth, screenHeight = WindowGetDimensions ("Root");
			
			-- check to see if AuctionStats installed
			if AuctionStats ~= nil and isMainTooltip then
				-- sAnchorTo = "top"
				-- sAnchorRel = "topleft"
				sAnchorWin = "AuctionToolTip"
				-- nWidthOffset = 300	-- default width for that add on window
			end
			if(tooltipY > mouseoverY and screenHeight > tooltipY + h + nRatingWindowHeight) then
				sAnchorTo = "top"
				sAnchorRel = "bottomleft"
				--WindowAddAnchor (windowName, "top", windowToAnchorTo, "bottomleft", nWidthOffset - ( w /2 ), 0);        
			else
				sAnchorTo = "bottom"
				sAnchorRel = "topleft"
				--WindowAddAnchor (windowName, "bottom", windowToAnchorTo, "topleft", nWidthOffset - ( w /2 ), 0);
			end
			
			WindowAddAnchor (windowName, sAnchorTo, sAnchorWin, sAnchorRel, nWidthOffset - ( w /2 ), 0);
			
			WindowSetAlpha( windowName, 1.0 )
			LabelSetText( windowName.."Info", l_bonus)
		end	
	end    
end


-- all the algorithms are global across servers and chars
-- with server.charname.algname for hidden ones
local function SaveCurrentData()
	local tempData = {}
	if(l_alg ~= nil) then
		for i,v in pairs (l_alg) do
			if (v.algname) then
				if(v.vis == 0) then
					-- Message("Saving "..v.algname)
					table.insert(tempData, v.algname)
				end
			end
		end
	end
	WARRatingBuster.SavedVars = l_alg
	WARRatingBuster.HiddenAlgs = tempData
end

local function LoadDefaultAlgData()
	l_alg = WARRatingBuster.SavedVars
	if(l_alg == nil) then
		l_alg = {}
	end	
	-- set to default visible, then turn off the ones not required in HiddenAlgs
	for i,v in pairs (l_alg) do
		if (v.algname) then
			v.vis = 1
		end
	end
	if(WARRatingBuster.HiddenAlgs ~= nil) then
		for i,v in pairs(WARRatingBuster.HiddenAlgs) do
			local iPos = findAlg(v)
			if(iPos > 0) then
				l_alg[iPos].vis = 0
			end	
		end
	end
end

local function ShowGeneralUsage()
	local l_stringtoshow = ""
	l_stringtoshow = "Usage: /wrb [?|[set <name> <algorithm>]|[toggle <name>]|[del <name>] ]\n"
	l_stringtoshow = l_stringtoshow .. "   set/amend algorithm: /wrb set <name> <stat=val,stat=val...>\n"
	l_stringtoshow = l_stringtoshow .. "   show/hide algorithm: /wrb toggle <name>\n"
	l_stringtoshow = l_stringtoshow .. "   remove algorithm: /wrb del <name>\n"
	if(l_alg ~= nil and #l_alg > 0) then
	l_stringtoshow = l_stringtoshow .. "   Algorithms:\n"
		for i,v in pairs (l_alg) do
			if (v.algname) then
				l_stringtoshow = l_stringtoshow .. "      " .. v.algname .. ": " .. v.alg .. "\n"
			end
		end
	end	
	Message(l_stringtoshow)
end

--------------------------------------
--   WARRatingBuster event handlers --
--------------------------------------

function WARRatingBuster.OnInitialize()
	-- hook tooltip windows for item -> set info.
	OldCreateItemTooltip = Tooltips.CreateItemTooltip
	Tooltips.CreateItemTooltip = WARRatingBuster.CreateItemTooltip

	OldAddExtraWindow = Tooltips.AddExtraWindow
	Tooltips.AddExtraWindow = WARRatingBuster.AddExtraWindow

	LibSlash.RegisterSlashCmd("wrb", WARRatingBuster.OnSlashCommand)
	
	for i=2,#l_WindowArr,1 do
		local windowName = l_WindowArr[i]
		-- TooltipWindowData[ windowName ] = { title = GetString( StringTables.Default.LABEL_CURRENT_ITEM ) }
		CreateWindow( windowName, false)
		-- LabelSetText( windowName.."Title", GetString( StringTables.Default.LABEL_CURRENT_ITEM ))
	end
	LoadDefaultAlgData()
end


function WARRatingBuster.Shutdown ()
	Tooltips.CreateItemTooltip = OldCreateItemTooltip
	Tooltips.AddExtraWindow = OldAddExtraWindow
	LibSlash.UnregisterSlashCmd("wrb")
end

function WARRatingBuster.OnSlashCommand(what)
	if(what ~= "") then
		local fields = explode(" ", what)
		-- /wrb set <name> <algorithm>
		-- /wrb toggle <name>
		-- /wrb del <name>
		if(#fields == 3) then
			fields[1] = tostring(fields[1]):lower()
			fields[2] = tostring(fields[2]):lower()
			fields[3] = tostring(fields[3]):lower()

			if(fields[1] == "set") then
				-- parse and store the algorithm
				setAlg(fields[2], fields[3])
				SaveCurrentData()
				Message(fields[2].. " saved.")
			else
				ShowGeneralUsage()
			end
		elseif(#fields == 2) then
			fields[1] = tostring(fields[1]):lower()
			fields[2] = tostring(fields[2]):lower()
			
			if(fields[1] == "toggle") then
				toggleAlg(fields[2])
				SaveCurrentData()
				Message(fields[2].. " toggled.")
			elseif(fields[1] == "del") then
				-- remove algorithm and save
				removeAlg(fields[2])
				SaveCurrentData()
				Message(fields[2].. " deleted.")
			else
				ShowGeneralUsage()
			end
		else
			ShowGeneralUsage()
		end
	else
		ShowGeneralUsage()
	end
end

-- CreateItemTooltip: create as normal then add our own
function WARRatingBuster.CreateItemTooltip( itemData, mouseoverWindow, anchor, disableComparison, extraText, extraTextColor, ignoreBroken )
	OldCreateItemTooltip( itemData, mouseoverWindow, anchor, disableComparison, extraText, extraTextColor, ignoreBroken )
	local l_thisWindow = l_WindowArr[1];
	l_CurrentWindowNumber = 2;
	AddStatWindow (l_thisWindow, "ItemTooltip", itemData, true)
end 

-- AddExtraWindow: create a comparison window
function WARRatingBuster.AddExtraWindow( strWindowName, strWindowToAnchorTo, oExtraData )
	OldAddExtraWindow( strWindowName, strWindowToAnchorTo, oExtraData )
	local l_thisWindow = l_WindowArr[l_CurrentWindowNumber];
	l_CurrentWindowNumber = l_CurrentWindowNumber + 1;
	AddStatWindow (l_thisWindow, strWindowName, oExtraData, false)          
end 

function WARRatingBuster.SaveData()
	SaveCurrentData()
end
