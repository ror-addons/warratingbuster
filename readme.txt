WAR Ratings Buster
==================
Author: john@veilofvisions.com
Readme version: 1.1
Readme date: 19 December 2009



Ratings Buster is an addon for Warhammer: Age of Reckonging which provides extra tooltip information for items which modify your characters statistics. 
This idea came from the World of Warcraft addon "Rating Buster" which can be found here http://wow.curse.com/downloads/wow-addons/details/rating-buster.aspx

It does the calculations on items with statistics, working out the parry%, etc. and displaying in a new tooltip.
Also, from v 0.2 onwards allows user to enter and save/modify algorithms that will calculate an items usefulness/rating accordingly. Like the World of Warcraft addon Pawn which can be found here http://wow.curse.com/downloads/wow-addons/details/pawn.aspx



Version History
---------------
v0.2.5	11/03/10	JXW	Added in a base comparison (without talis slotted)
v0.2.3	22/09/09	JXW	Fixes to visibility defaults
v0.2.2	23/08/09	JXW	More stupid errors fixed.
v0.2.1	21/08/09	JXW	Made stupid errors in v0.2 
v0.2	17/08/09	JXW	1.3.0b code. PAWN-esque attempt 1
v0.1.3	25/04/09	JXW	Removed name & item from window, couple of startup errors fixed.
v0.1.2	20/04/09	JXW	AuctionStats window aware back in
v0.1.1	18/04/09	JXW	Comparison tooltips working
v0.1.0	17/04/09	JXW	Changed mod to be easier 1.2.1 compatible. Reduced some output text length
v0.0.3	14/04/09	JXW	Sanitised code: local functions rather than global (better programming). Abstracted summary text to seperate function for future changes. Lots of old debug comments and code removed
v0.0.2	31/01/09	JXW	Remove string appending and create summary table to report from
v0.0.1	14/01/09	JXW	First working version, nasty code


Information
-----------
This addon requires LibSlash from version 0.2 onwards for the ability to add, edit and delete user defined rating systems.
LibSlash can be found here: http://war.curse.com/downloads/war-addons/details/libslash.aspx


User Defined Ratings
--------------------
Similar to, and inspired by, the World of Warcraft addon "Pawn". This is in a very early stage of development at the moment, there is no UI at the moment to allow you to enter values easily, that's still yet to come.
But I have added some functionality in for the calculation and display of these uder defined "algorithms" or ratings.

The ratings are stored globally, but each character you have will have their own hidden/visible settings.


Slash Commands
--------------
Slash commands for manipulating the user defined ratings have been added in v0.2 onwards.
These are:

/wrb [?|[set <name> <algorithm>]|[toggle <name>]|[del <name>] ]

to create / set: /wrb set <name> <stat=val,stat=val...>

to toggle visibility on/off for this character: /wrb toggle <name>

to delete a user defined rating: /wrb del <name>

to view these options and a list of currently stored algorithms, use either
/wrb 
/wrb ?


Creating / Setting User Defined Algorithms
------------------------------------------
Until I create a UI window for all the values available, the only way to enter these is via the command line:
/wrb set <name> <stat=val,stat=val...>

Example:
/wrb set strength s=1
This will create/re-set the algorithm named "strength" to calculate s (strength, see below) to equal 1 point per point on the item.

This will be shown under the normal WAR Rating Buster information as "strength: 12.00" for instance if the item had +12 Strength bonus.

Allowing the use of the command line first is so that people can easily post their own versions of algorithms online and others can copy and paste them into their client without having to go through the sometimes tedious process of thinking what values to allocate to each statistic.


Here's a list of the currently available statistics.

Statistic			variable to use in algorithm
--------------			----------------------------
Strength			s
Weapon Skill			ws
Ballistic Skill			bs
Intelligence			int
Toughness			t
Wounds				w
Initiative			i
Willpower			wp

Spiritual Resist		spi
Corporeal Resist		cor
Elemental Resist		ele

Armour				arm
Block Rating			brt

Plus block percent		pblk
Plus parry percent		ppar
Plus dodge/evade percent	pdod
Plus disrupt percent		pdis
Plus armour penetration		papen

Reduced chance to be blocked	bblk
Reduced chance to be parried	bpar
Reduced chance to be dodged	bdod
Reduced chance to be disrupted	bdis
Reduced chance to be crit	bcrit
Reduced armour penetration	rapen

AP regen per second		aps
Morale per second		mps
HP regen per 4 seconds		hps

Increased hate generation	ihat
Decreased hate generation	dhat

Melee critical chance		melc
Ranged critical chance		ranc
Magic critical chance		magc
Heal critical chance		heac

Melee power			melp
Ranged power			ranp
Magic power			magp
Heal power			heap


As I continue to improve the code and functions I will add more statistics into the list.
Currently I need to find an item that has +armour penetration as a bonus, not a set bonus.

Examples:
/wrb set rvrheal t=1,w=1.1,wp=1,spi=0.1,cor=0.15,ele=0.15,arm=0.1,pdod=5,pdis=5,rapen=5,heac=16,heap=0.75
