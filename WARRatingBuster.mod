<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
    <UiMod name="WARRatingBuster" version="0.2.5" date="11/03/2010" >
        <Author name="John @ Veil of Visions" email="john@veilofvisions.com" />
        <Description text="A tooltip info mod giving more data about the stats on items and allowing users to create their own algorithms to calculate ratings on items." />
        <VersionSettings gameVersion="1.3.6" windowsVersion="1.0" savedVariablesVersion="1.1" />
        <Dependencies>
		<Dependency name="LibSlash" />
        </Dependencies>
        <Files>        
            <File name="WARRatingBuster.lua" />
            <File name="Tooltip.xml" />
        </Files>
        <SavedVariables>
        	<SavedVariable name="WARRatingBuster.SavedVars" global="true"/>
        	<SavedVariable name="WARRatingBuster.HiddenAlgs" global="false" />
        </SavedVariables>
        <OnInitialize>
		<CallFunction name="WARRatingBuster.OnInitialize" />
		<CreateWindow name="RatingToolTip1" show="false" />
	</OnInitialize>
        <OnShutdown>
		<CallFunction name="WARRatingBuster.SaveData" />
        </OnShutdown>
	<WARInfo>
	    <Categories>
		<Category name="ITEMS_INVENTORY" />
	    </Categories>
	    <Careers>
		<Career name="BLACKGUARD" />
		<Career name="WITCH_ELF" />
		<Career name="DISCIPLE" />
		<Career name="SORCERER" />
		<Career name="IRON_BREAKER" />
		<Career name="SLAYER" />
		<Career name="RUNE_PRIEST" />
		<Career name="ENGINEER" />
		<Career name="BLACK_ORC" />
		<Career name="CHOPPA" />
		<Career name="SHAMAN" />
		<Career name="SQUIG_HERDER" />
		<Career name="WITCH_HUNTER" />
		<Career name="KNIGHT" />
		<Career name="BRIGHT_WIZARD" />
		<Career name="WARRIOR_PRIEST" />
		<Career name="CHOSEN" />
		<Career name="MARAUDER" />
		<Career name="ZEALOT" />
		<Career name="MAGUS" />
		<Career name="SWORDMASTER" />
		<Career name="SHADOW_WARRIOR" />
		<Career name="WHITE_LION" />
		<Career name="ARCHMAGE" />
	    </Careers>
	</WARInfo>
    </UiMod>    
</ModuleFile>